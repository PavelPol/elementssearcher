# ElementsSearcher

Installation

0. Create database with UTF-8 character set.
1. Copy Env/db.sample.php as Env/db.php and change database credentials.
2. Composer install, add x flag to file bin/app (chmod +x bin/app)
3. Process migration using command 'php bin/app migrate'
4. Use web directory as Apache root.