<?php
/**
 * @var array $variables
 */
?>
<div class="row">
    <div class="col-sm-offset-3 col-sm-6">
        <div class="well">
            <form class="form-horizontal" id="search-form" action="/search" method="post">
                <h2>Search form</h2>
                <div class="form-group">
                    <input type="url" class="form-control" name="url" placeholder="Site url" required/>
                </div>
                <div class="form-group">
                    <select name="type" class="form-control">
                        <option value="link">Links</option>
                        <option value="image">Images</option>
                        <option value="text">Text</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea name="text" placeholder="Text" class="form-control hidden"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <a href="/results" class="btn btn-default">Results page</a>
                </div>
            </form>
        </div>
    </div>
</div>
