<?php
/**
 * @var array $variables
 */
/** @var \Model\Site[] $sites */
$sites = $variables['sites'];
?>
<div class="row">
    <div class="col-sm-offset-1 col-sm-10">
        <div class="well results">
            <?php foreach($sites as $site): ?>
                <h3><?= htmlspecialchars($site->url) ?></h3>

                <?php $links = $site->getLinks() ?>
                <h4>Links: <?= count($links) ?></h4><br/>
                <?php foreach($links as $link): ?>
                    <span><?= htmlspecialchars($link->data) ?></span><br/>
                <?php endforeach ?>

                <?php $images = $site->getImages() ?>
                <h4>Images: <?= count($images) ?></h4><br/>
                <?php foreach($images as $image): ?>
                    <span><?= htmlspecialchars($image->data) ?></span><br/>
                <?php endforeach ?>

                <?php $texts = $site->getTexts() ?>
                <h4>Texts: <?= count($texts) ?></h4><br/>
                <?php foreach($texts as $text): ?>
                    <span><?= htmlspecialchars($text->data) ?></span><br/>
                <?php endforeach ?>

            <?php endforeach ?>
        </div>
    </div>
</div>
