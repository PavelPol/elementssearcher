<?php

namespace Console;

use Core\Console\Command;
use Core\Console\Result;
use Core\Interfaces\ResultInterface;


class Migrate extends Command
{
    public function run(): ResultInterface
    {
        $db = $this->core->getDb();

        $result = new Result();

        $sql = <<<SQL
        CREATE TABLE IF NOT EXISTS  `site` (
        id INT(11) AUTO_INCREMENT PRIMARY KEY,
        url VARCHAR(100) NOT NULL,
        UNIQUE KEY(`url`));"
SQL;
        if (!$db->exec($sql)) {
            $result->addMessage("Fail create table `site`");
        } else {
            $result->addMessage("Successful created table `site`");
        }

        $sql = <<<SQL
        CREATE TABLE IF NOT EXISTS `result` (
        id INT(11) AUTO_INCREMENT PRIMARY KEY,
        site_id INT(11) NOT NULL,
        type VARCHAR(50) NOT NULL,
        data TEXT,
        INDEX site_id_idx (`site_id`),
             FOREIGN KEY(`site_id`) references `site` (`id`)
             ON DELETE cascade);"
SQL;
        if (!$db->exec($sql)) {
            $result->addMessage("Fail create table `result`");
        } else {
            $result->addMessage("Successful created table `result`");
        }

        return $result;
    }
}
