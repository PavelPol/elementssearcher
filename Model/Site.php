<?php

namespace Model;

use Core\Model;

/**
 * Class Site
 * @package Model
 *
 * @property int $id
 * @property string $url
 */
class Site extends Model
{
    public static function table(): string
    {
        return 'site';
    }

    public function addResult(array $data): Site
    {
        $data['site_id'] = $this->id;

        (new Result($this->db, $data))->save();

        return $this;
    }

    public function findByUrl(string $url): ?Site
    {
        /** @var Site|null $site */
        $site = $this->findOne(['url' => $url]);
        return $site;
    }

    /**
     * @return Result[]
     */
    public function getLinks(): array
    {
        return (new Result($this->db))->findAll([
            'site_id' => $this->id,
            'type' => 'link'
        ]);
    }

    /**
     * @return Result[]
     */
    public function getImages(): array
    {
        return (new Result($this->db))->findAll([
            'site_id' => $this->id,
            'type' => 'image'
        ]);
    }

    /**
     * @return Result[]
     */
    public function getTexts(): array
    {
        return (new Result($this->db))->findAll([
            'site_id' => $this->id,
            'type' => 'text'
        ]);
    }
}
