<?php

namespace Model;

use Core\Model;


/**
 * Class Result
 * @package Model
 *
 * @property int id
 * @property int site_id
 * @property string $data
 */
class Result extends Model
{
    public static function table(): string
    {
        return 'result';
    }
}
