<?php

namespace Core;


use Core\Interfaces\CoreInterface;
use Core\Interfaces\RegistryInterface;

abstract class Component
{
    /** @var CoreInterface $core */
    protected $core;

    /** @var RegistryInterface $registry */
    protected $registry;

    public function __construct(
        CoreInterface $core,
        RegistryInterface $registry
    ) {
        $this->core = $core;
        $this->registry = $registry;
    }

    public static function getComponentName(): string
    {
        $path = explode('\\', get_called_class());
        return strtolower(array_pop($path));
    }
}
