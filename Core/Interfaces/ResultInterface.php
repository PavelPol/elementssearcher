<?php

namespace Core\Interfaces;

interface ResultInterface
{
    public function __toString();

    public function send(): void;
}
