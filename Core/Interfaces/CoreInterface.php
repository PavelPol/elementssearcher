<?php

namespace Core\Interfaces;

use Core\Component;


interface CoreInterface
{
    public function run(): void;

    public function createComponent(string $class): Component;

    public function getDb(): \PDO;
}
