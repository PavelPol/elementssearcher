<?php

namespace Core\Interfaces;


interface RegistryInterface
{
    public function registerCommand(string $name, string $class): self;

    public function retrieveCommand(string $name): string;

    public function registerController(string $name, string $class): self;

    public function retrieveController(string $name): string;
}
