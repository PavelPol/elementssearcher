<?php

namespace Core;


abstract class Model
{
    protected $db;

    protected $data = [];

    protected $isNewRecord = true;

    public function __construct(
        \PDO $db,
        array $data = []
    ) {
        $this->db = $db;
        $this->data = $data;
    }

    public abstract static function table(): string;

    public function findOne(array $where): ?Model
    {
        $table = static::table();

        $_where = array_map(function($field) {
            return "{$field} = :{$field}";
        }, array_keys($where));
        $_where = implode(', ', $_where);

        $statement = $this->db->prepare(
            "SELECT * FROM {$table} WHERE {$_where}"
        );
        $statement->execute($where);
        if (!$data = $statement->fetch()) {
            return null;
        }

        $class = get_called_class();
        return new $class($this->db, $data);
    }

    public function findAll(array $where = []): array
    {
        $table = static::table();

        $_where = array_map(function($field) {
            return "{$field} = :{$field}";
        }, array_keys($where));
        $_where = implode(' AND ', $_where);

        $statement = $this->db->prepare(
            "SELECT * FROM  {$table} " . ($_where ? "WHERE {$_where};" : ';')
        );

        $statement->execute($where);
        $data = $statement->fetchAll();

        $class = get_called_class();
        $result = [];
        foreach ($data as $item) {
            $result[] = new $class($this->db, $item);
        }

        return $result;
    }

    public function save()
    {
        $table = static::table();

        $_set = array_map(function($field) {
            return "{$field} = :{$field}";
        }, array_keys($this->data));
        $_where = implode(', ', $_set);

        $_fields = implode(', ', array_keys($this->data));
        $_values = implode(', ', array_map(function($value) {
            return ":$value";
        }, array_keys($this->data)));

        $statement = $this->db->prepare(
            $this->isNewRecord
                ? "INSERT INTO  {$table} ({$_fields}) VALUES ({$_values});"
                : "UPDATE {$table} SET {$_set} WHERE id = :id;"
        );

        $statement->execute($this->data);

        $this->id = $this->db->lastInsertId();
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }
}
