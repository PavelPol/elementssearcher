<?php

namespace Core\Console;

use Core\Component;
use Core\Interfaces\ResultInterface;
use Core\Interfaces\RegistryInterface;
use Core\Interfaces\CoreInterface;


abstract class Command extends Component
{
    public abstract function run(): ResultInterface;

    public function getParameters(): array
    {

    }
}

// todo base command, get parameters