<?php

namespace Core\Console;

use Core\Interfaces\ResultInterface;


class Result implements ResultInterface
{
    private $text;

    public function __construct(string $text = '')
    {
        $this->text = $text;
    }

    public function addMessage(string $string): Result
    {
        $this->text .= $string . "\n";
        return $this;
    }

    public function __toString()
    {
        return $this->text;
    }

    public function send(): void
    {
        echo $this . "\n";
    }
}
