<?php

namespace Core\Console\Exceptions;

use Throwable;


class InvalidCommandException extends \Exception
{
    public function __construct(
        $message = 'Invalid command',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
