<?php

namespace Core\Console\Exceptions;

use Throwable;


class InvalidControllerException extends \Exception
{
    public function __construct(
        $message = 'Page is not found',
        $code = 404,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
