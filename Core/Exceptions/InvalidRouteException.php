<?php

namespace Core\Console\Exceptions;


use Throwable;

class InvalidRouteException extends \Exception
{
    public function __construct(
        $message = 'Please, specify the route',
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
