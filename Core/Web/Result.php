<?php

namespace Core\Web;

use Core\Interfaces\ResultInterface;


class Result implements ResultInterface
{
    /** @var int $redirectCode */
    private $redirectCode;

    /** @var string $redirectUrl */
    private $redirectUrl;

    /** @var string $content */
    private $content;

    public function __construct(string $content = '')
    {
        $this->content = $content;
    }

    public function setContent(string $content): Result
    {
        $this->content = $content;
        return $this;
    }

    public function redirect(string $url, int $code = 302): Result
    {
        $this->redirectCode = $code;
        $this->redirectUrl = $url;
        return $this;
    }

    public function __toString()
    {
        return $this->content;
    }

    public function send(): void
    {
        if ($this->redirectUrl) {
            header("Location: {$this->redirectUrl}", true, $this->redirectCode);
        } else {
            echo $this;
        }
    }
}
