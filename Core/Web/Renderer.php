<?php

namespace Core\Web;

class Renderer
{
    private $layout = 'layout';

    public function render(string $view, array $variables = [])
    {
        $content = $this->renderContent($view, $variables);
        return $this->renderLayout($content);
    }

    public function renderLayout(string $content)
    {
        $variables = [
            'content' => $content
        ];

        ob_start();
        require __DIR__ . '/../../View/' . $this->layout . '.php';
        return ob_get_clean();
    }

    protected function renderContent(string $view, array $variables = [])
    {
        ob_start();
        require __DIR__ . '/../../View/' . $view . '.php';
        return ob_get_clean();
    }
}
