<?php

namespace Core\Web;

use Core\Component;
use Core\Interfaces\CoreInterface;
use Core\Interfaces\RegistryInterface;
use Core\Interfaces\ResultInterface;

abstract class Controller extends Component
{
    protected $renderer;

    public function __construct(CoreInterface $core, RegistryInterface $registry)
    {
        parent::__construct($core, $registry);

        $this->renderer = new Renderer();
    }

    public abstract function run(): ResultInterface;

    public function getPost($name)
    {
        return $_POST[$name] ?? null;
    }
}
