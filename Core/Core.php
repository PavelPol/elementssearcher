<?php

namespace Core;

use Core\Console\Command;
use Core\Console\Exceptions\InvalidRouteException;
use Core\Interfaces\CoreInterface;
use Core\Web\Controller;
use HaydenPierce\ClassFinder\ClassFinder;


class Core implements CoreInterface
{
    /** @var bool $isConsole */
    private $isConsole = false;

    private $registry;

    private  $db;

    public function __construct()
    {
        $this->registry = new Registry();
        $this->registerComponents();
    }

    public function run(): void
    {
        try {
            $route = $this->getRoute();

            if ($this->isConsole()) {
                /** @var Command $command */
                $command = $this->createComponent($this->registry->retrieveCommand($route));
                $result = $command->run();
            } else {
                /** @var Controller $controller */
                $controller = $this->createComponent($this->registry->retrieveController($route));
                $result = $controller->run();
            }
            $result->send();
        } catch (\Exception $e) {
            if ($e->getCode() != 0) {
                http_response_code($e->getCode());
            }
            echo $e->getMessage() . "\n";
        }
    }

    /**
     * @return string
     * @throws InvalidRouteException
     */
    public function getRoute(): string
    {
        if ($this->isConsole() && isset($_SERVER['argv'][1])) {
            return $_SERVER['argv'][1];
        } elseif (!$this->isConsole()) {
            return $_SERVER['REQUEST_URI'] == '/' ? 'index' : ltrim($_SERVER['REQUEST_URI'], '/');
        }
        throw new InvalidRouteException();
    }

    public function isConsole(): bool
    {
        return $this->isConsole;
    }

    public function setIsConsole(bool $flag = true): self
    {
        $this->isConsole = $flag;
        return $this;
    }

    public function getDb(): \PDO
    {
        if ($this->db === null) {
            $configs = require __DIR__ . '/../Env/db.php';
            $this->db = new \PDO($configs['dsn'], $configs['user'], $configs['password']);
        }

        return $this->db;
    }

    public function registerComponents()
    {
        ClassFinder::disablePSR4Support();

        $commands = ClassFinder::getClassesInNamespace('Console', ClassFinder::RECURSIVE_MODE);
        foreach ($commands as $command) {
            /** @var Component|string $command */
            $this->registry->registerCommand($command::getComponentName(), $command);
        }

        $controllers = ClassFinder::getClassesInNamespace('Controller', ClassFinder::RECURSIVE_MODE);

        foreach ($controllers as $controller) {
            /** @var Component|string $controller */
            $this->registry->registerController($controller::getComponentName(), $controller);
        }
    }

    public function createComponent(string $class): Component
    {
        return new $class($this, $this->registry);
    }
}
