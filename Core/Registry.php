<?php

namespace Core;


use Core\Console\Exceptions\InvalidCommandException;
use Core\Console\Exceptions\InvalidControllerException;
use Core\Interfaces\RegistryInterface;


class Registry implements RegistryInterface
{
    /** @var array $commands */
    protected $commands = [];

    /** @var array $controllers */
    protected $controllers = [];

    public function registerCommand(string $name, string $class): RegistryInterface
    {
        $this->commands[$name] = $class;
        return $this;
    }


    public function retrieveCommand(string $name): string
    {
        if (isset($this->commands[$name])) {
            return $this->commands[$name];
        }

        throw new InvalidCommandException();
    }

    public function registerController(string $name, string $class): RegistryInterface
    {
        $this->controllers[$name] = $class;
        return $this;
    }

    public function retrieveController(string $name): string
    {
        if (isset($this->controllers[$name])) {
            return $this->controllers[$name];
        }

        throw new InvalidControllerException();
    }
}
