<?php

namespace Component;

use Core\Component;
use Curl\Curl;


class Parser extends Component
{
    public function parseSite(string $url, string $type, string $text = null): array
    {
        $content = $this->getContent($url);

        switch ($type) {
            case 'link':
                return $this->parseLinks($content);
            case 'image':
                return $this->parseImages($content);
            case 'text':
                return $this->parseText($content, $text);
            default:
                return [];
        }
    }

    protected function getContent(string $url): string
    {
        $curl = new Curl();
        $curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $content = $curl->get($url)->getResponse();
        $curl->close();

        return $content;
    }

    protected function parseLinks(string $content): array
    {
        $matches = null;
        preg_match_all(
            '/<a\s+(?:[^"\'>]+|"[^"]*"|\'[^\']*\')*href=("[^"]+"|\'[^\']+\'|[^<>\s]+)/i',
                    $content,
            $matches
        );

        return $matches[1] ?? [];
    }

    protected function parseImages(string $content): array
    {
        $matches = null;
        preg_match_all('/< *img[^>]*src *= *["\']?([^"\']*)/i', $content, $matches);

        return $matches[1] ?? [];
    }

    protected function parseText(string $content, string $text): array
    {
        $matches = null;
        preg_match_all('/' . preg_quote($text) . '/i', $content, $matches);

        return $matches[1] ?? [];
    }
}
