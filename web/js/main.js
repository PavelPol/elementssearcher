$(function () {
    'use strict';

    $('#search-form').submit(function(e) {
        e.preventDefault();

        var $form = $(this);

        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            dataType: 'json',
            data: $form.serialize(),
            success: function (response) {
                if (response.success) {
                    document.location.href = response.url;
                } else {
                    alert(response.message);
                }
            },
            error: function (response) {
                console.log(response);
            }
        });
    });

    $('#search-form [name="type"]').change(function() {
        var $text = $('#search-form [name="text"]');

        if ($(this).val() == 'text') {
            $text.prop('required', true);
            $text.removeClass('hidden');
        } else {
            $text.prop('required', false);
            $text.val('').addClass('hidden');
        }
    }).trigger('change');

});
