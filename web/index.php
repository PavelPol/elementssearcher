<?php

if (file_exists(__DIR__ . '/../Env/config.php')) {
    require_once __DIR__ . '/../Env/config.php';
}

require_once __DIR__ . '/../Env/db.php';

require_once __DIR__ . '/../vendor/autoload.php';

(new \Core\Core)->run();
