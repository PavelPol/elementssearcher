<?php

namespace Controller;

use Core\Interfaces\ResultInterface;
use Core\Web\Controller;
use Core\Web\Result;

class Index extends Controller
{
    public function run(): ResultInterface
    {
        return new Result($this->renderer->render('index', [

        ]));
    }
}
