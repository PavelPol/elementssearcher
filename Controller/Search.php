<?php

namespace Controller;

use Component\Parser;
use Core\Interfaces\ResultInterface;
use Core\Web\Controller;
use Core\Web\Result;
use Model\Site;


class Search extends Controller
{
    public function run(): ResultInterface
    {
        $url = $this->getPost('url');
        $type = $this->getPost('type');

        if (!$url || !$type || ($type == 'text' && !$this->getPost('text'))) {
            return (new Result(json_encode([
                'success' => false,
                'message' => 'Check the form values'
            ])));
        }

        if (!in_array($type, ['link', 'image', 'text'])) {
            return (new Result(json_encode([
                'success' => false,
                'message' => 'Invalid type'
            ])));
        }

        $this->process($url, $type);

        return (new Result(json_encode([
            'success' => true,
            'url' => '/results'
        ])));
    }

    public function process(string $url, string $type): void
    {
        $parser = $this->core->createComponent(Parser::class);
        $data = $parser->parseSite($url, $type, $this->getPost('text'));

        $site = (new Site($this->core->getDb()))->findByUrl($url);
        if (!$site) {
            $site = new Site($this->core->getDb(), [
                'url' => $url
            ]);
            $site->save();
        }

        foreach ($data as $item) {
            $site->addResult([
                'type' => $type,
                'data' => $item
            ]);
        }
    }
}
