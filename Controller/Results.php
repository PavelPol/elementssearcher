<?php

namespace Controller;

use Core\Interfaces\ResultInterface;
use Core\Web\Controller;
use Core\Web\Result;
use Model\Site;

class Results extends Controller
{
    public function run(): ResultInterface
    {
        $sites = (new Site($this->core->getDb()))->findAll();

        return new Result($this->renderer->render('results', [
            'sites' => $sites
        ]));
    }
}
